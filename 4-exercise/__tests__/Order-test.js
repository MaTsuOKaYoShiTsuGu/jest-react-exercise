import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axiosMock from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { getByLabelText, getByTestId } = render(<OrderComponent />);
  const input = getByLabelText('number-input');
  const button = getByLabelText('submit-button');
  const p = getByTestId('status');
  // Mock数据请求
  axiosMock.get.mockResolvedValue({ data: { status: '200' } });
  // 触发事件
  fireEvent.change(input, { target: { value: '2333' } });
  await fireEvent.click(button);
  // 给出断言
  await expect(p).toHaveTextContent('200');
  // --end->
});
